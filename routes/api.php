<?php

use App\Http\Controllers\Api\v1\Brand\BrandController;
use App\Http\Controllers\Api\v1\Car\CarController;
use App\Http\Controllers\Api\v1\Model\ModelController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['as' => 'api.'], function () {
    Route::get('/brands', [BrandController::class, 'index'])->name('brands');
    Route::get('/models', [ModelController::class, 'index'])->name('models');

    Route::group(['prefix' => 'cars', 'as' => 'cars.'], function () {
        Route::get('/', [CarController::class, 'index'])->name('index');
    });

    Route::group(['middleware' => 'auth:sanctum'], function () {
        Route::get('/user', function (Request $request) {
            return $request->user();
        });

        Route::group(['middleware' => 'car', 'prefix' => 'car', 'as' => 'car.'], function () {
            Route::post('/create', [CarController::class, 'create'])->name('create');
            Route::get('/{car}', [CarController::class, 'show'])->name('show');
            Route::post('/update/{car}', [CarController::class, 'update'])->name('update');
            Route::post('/delete/{car}', [CarController::class, 'delete'])->name('delete');
        });
    });
});
