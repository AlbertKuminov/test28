<?php

namespace App\Http\Requests\Api\v1\Car;

use App\Models\CarBrand;
use App\Models\CarModel;
use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;

/**
 * @property int|null year_of_issue
 * @property int|null mileage
 * @property string|null color
 *
 * @property CarBrand $brand
 * @property CarModel $model
 */
class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    #[ArrayShape([
        'brand' => "string[]",
        'model' => "string[]",
        'year_of_issue' => "string[]",
        'mileage' => "string[]",
        'color' => "string[]"
    ])]
    public function rules(): array
    {
        return [
            'brand' => ['required', 'integer', 'exists:car_brands,id'],
            'model' => ['required', 'integer', 'exists:car_models,id'],
            'year_of_issue' => ['nullable', 'integer', 'min:1900'],
            'mileage' => ['nullable', 'integer', 'max:2000000'],
            'color' => ['nullable', 'string', 'min:1'],
        ];
    }
}
