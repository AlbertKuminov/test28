<?php

namespace App\Http\Services;

use App\Models\CarModel;
use Illuminate\Database\Eloquent\Collection;

class ModelService
{
    use ServiceInstance;

    /**
     * @return Collection
     */
    public function getAllModels(): Collection
    {
        return CarModel::all();
    }
}
