<?php

namespace App\Http\Services;

use App\Models\CarBrand;
use Illuminate\Database\Eloquent\Collection;

class BrandService
{
    use ServiceInstance;

    /**
     * @return Collection
     */
    public function getAllBrands(): Collection
    {
        return CarBrand::all();
    }
}
