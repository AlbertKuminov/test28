<?php

namespace App\Http\Services;

use App\Http\Requests\Api\v1\Car\CarRequest;
use App\Models\Car;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CarService
{
    use ServiceInstance;

    /**
     * @return Collection
     */
    public function getAllCars(): Collection
    {
        return Car::all();
    }

    /**
     * @param CarRequest $carRequest
     *
     * @return Car
     */
    public function createNewCar(CarRequest $carRequest): Car
    {
        $car = new Car();

        return $this->updateCarModel($carRequest, $car);
    }

    /**
     * @param Car $car
     *
     * @return Model
     */
    public function showCar(Car $car): Model
    {
        return Car::query()->find($car->id);
    }

    /**
     * @param CarRequest $carRequest
     *
     * @return Car
     */
    public function updateCar(CarRequest $carRequest, Car $car): Car
    {
        /** @var Car $carModel */
        $carModel = Car::query()->find($car->id);

        return $this->updateCarModel($carRequest, $carModel);
    }

    /**
     * @param CarRequest $carRequest
     * @param Car $carModel
     * @return Car
     */
    public function updateCarModel(CarRequest $carRequest, Car $carModel): Car
    {
        $carModel->user_id = Auth::id();
        $carModel->brand = $carRequest->brand;
        $carModel->carModel = $carRequest->model;
        $carModel->year_of_issue = $carRequest->year_of_issue ?? null;
        $carModel->mileage = $carRequest->mileage ?? null;
        $carModel->color = $carRequest->color ?? null;
        $carModel->save();

        return $carModel;
    }

    /**
     * @param Car $car
     *
     * @return void
     */
    public function deleteCar(Car $car): void
    {
        $carModel = Car::query()->find($car->id);
        $carModel->delete();
    }
}
