<?php

namespace App\Http\Resources;

use App\Models\CarBrand;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

class BrandResource extends JsonResource
{
    private function getResource(): CarBrand
    {
        return $this->resource;
    }

    #[ArrayShape(['name' => "string"])]
    public function toArray($request): array
    {
        $brand = $this->getResource();

        return [
            'name' => $brand->name
        ];
    }
}
