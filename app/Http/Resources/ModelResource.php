<?php

namespace App\Http\Resources;

use App\Models\CarModel;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

class ModelResource extends JsonResource
{
    private function getResource(): CarModel
    {
        return $this->resource;
    }

    #[ArrayShape(['name' => "string", 'brand' => "string"])]
    public function toArray($request): array
    {
        $model = $this->getResource();

        return [
            'name' => $model->name,
            'brand' => $model->brand->name
        ];
    }
}
