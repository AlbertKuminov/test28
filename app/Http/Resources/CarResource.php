<?php

namespace App\Http\Resources;

use App\Models\Car;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;

class CarResource extends JsonResource
{
    private function getResource(): Car
    {
        return $this->resource;
    }


    #[ArrayShape([
        'user' => "string",
        'brand' => "string",
        'model' => "string",
        'year_of_issue' => "int|null",
        'mileage' => "int|null", 'color' => "null|string"
    ])]
    public function toArray($request): array
    {
        $model = $this->getResource();

        return [
            'user' => $model->user->name,
            'brand' => $model->brand->name,
            'model' => $model->carModel->name,
            'year_of_issue' => $model->year_of_issue ?? null,
            'mileage' => $model->mileage ?? null,
            'color' => $model->color ?? null,
        ];
    }
}
