<?php

namespace App\Http\Middleware;

use App\Exceptions\ApiCarException;
use App\Models\Car;
use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CarMiddleware
{
    /**
     * @throws ApiCarException
     */
    public function handle(Request $request, Closure $next): Response|RedirectResponse|JsonResponse
    {
        /** @var Car $booking */
        $car = $request->route()->parameter('car');

        if ($car && $car->user_id !== auth_user_or_fail()->getKey()) {
            throw new ApiCarException();
        }

        return $next($request);
    }
}
