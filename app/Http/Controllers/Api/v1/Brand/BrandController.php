<?php

namespace App\Http\Controllers\Api\v1\Brand;

use App\Http\Controllers\Controller;
use App\Http\Resources\BrandResource;
use App\Http\Services\BrandService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class BrandController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $brandService = BrandService::create();
        $brands = $brandService->getAllBrands();

        return BrandResource::collection($brands);
    }
}
