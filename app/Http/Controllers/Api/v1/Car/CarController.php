<?php

namespace App\Http\Controllers\Api\v1\Car;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\v1\Car\CarRequest;
use App\Http\Resources\CarResource;
use App\Http\Services\CarService;
use App\Models\Car;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CarController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $carService = CarService::create();
        $cars = $carService->getAllCars();

        return CarResource::collection($cars);
    }

    /**
     * @param CarRequest $carRequest
     *
     * @return JsonResponse
     */
    public function create(CarRequest $carRequest): JsonResponse
    {
        $carService = CarService::create();
        $car = $carService->createNewCar($carRequest);

        return response()->json([
            'message' => 'Данные успешно сохранены.',
            'data' => CarResource::make($car),
        ]);
    }

    /**
     * @param Car $car
     *
     * @return CarResource
     */
    public function show(Car $car): CarResource
    {
        $carService = CarService::create();
        $car = $carService->showCar($car);

        return CarResource::make($car);
    }

    /**
     * @param CarRequest $carRequest
     * @param Car $car
     *
     * @return JsonResponse
     */
    public function update(CarRequest $carRequest, Car $car): JsonResponse
    {
        $carService = CarService::create();
        $car = $carService->updateCar($carRequest, $car);

        return response()->json([
            'message' => 'Данные успешно сохранены.',
            'data' => CarResource::make($car),
        ]);
    }

    /**
     * @param Car $car
     *
     * @return JsonResponse
     */
    public function delete(Car $car): JsonResponse
    {
        $carService = CarService::create();
        $carService->deleteCar($car);

        return response()->json([
            'message' => 'Данные успешно удалены.',
        ]);
    }
}
