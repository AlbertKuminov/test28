<?php

namespace App\Http\Controllers\Api\v1\Model;

use App\Http\Controllers\Controller;
use App\Http\Resources\ModelResource;
use App\Http\Services\ModelService;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ModelController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function index(): AnonymousResourceCollection
    {
        $modelService = ModelService::create();
        $models = $modelService->getAllModels();

        return ModelResource::collection($models);
    }
}
