<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property-read string name
 */
class CarBrand extends Model
{
    use HasFactory;

    public function models(): HasMany
    {
        return $this->HasMany(CarModel::class);
    }

    public function cars(): HasMany
    {
        return $this->HasMany(Car::class);
    }
}
