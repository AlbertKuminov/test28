<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int id
 * @property int user_id
 * @property int|null year_of_issue
 * @property int|null mileage
 * @property string|null color
 *
 * @property User $user
 * @property CarBrand $brand
 * @property CarModel $carModel
 */
class Car extends Model
{
    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(CarBrand::class);
    }

    public function model(): BelongsTo
    {
        return $this->belongsTo(CarModel::class);
    }
}
