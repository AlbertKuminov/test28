<?php

namespace App\Exceptions;

use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ApiCarException extends ApiException
{
    protected $code = ResponseAlias::HTTP_NOT_FOUND;
    protected $message = 'Автомобиль не найдено.';
}
